package com.hendisantika.springbootsecurityssooauth2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-sso-oauth2
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/11/19
 * Time: 05.59
 */
@Controller
public class HomeController {
    @GetMapping("/")
    public String home() {
        return "index";
    }

}