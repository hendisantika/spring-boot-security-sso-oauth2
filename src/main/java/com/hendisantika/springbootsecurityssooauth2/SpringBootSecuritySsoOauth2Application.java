package com.hendisantika.springbootsecurityssooauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSecuritySsoOauth2Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecuritySsoOauth2Application.class, args);
    }

}
