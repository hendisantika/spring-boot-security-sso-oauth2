# Spring Boot Security SSO with OAuth2

## Introduction

Here we will see Spring Boot Security Example – Single Sign On using OAuth 2. 
Single Sign On or simply SSO is a property of access control of multiple related, 
yet independent, software systems, where a user logs in with a single set of credentials 
(username and password) to gain access. Here we will use Spring Boot 2.2.5.RELEASE version.

We will let client access our App by granting access through Github OAuth 2 API. 
You can also use other OAuth 2 API, such as, Google, Facebook etc. 
Even you can create your own OAuth 2 server.

For this we need to first register the App into Github. 
It is assumed that you have a Github account.

You may also like to read Spring Security Examples.

## What is OAuth 2?

OAuth is a protocol with which a 3-party app can access your data stored in another website without your account and password. 
For more information on OAuth 2 then you can read https://oauth.net/2/.

## Prerequisites

Knowledge on Java, Spring Boot, Spring Security.

Github Account, Spring Boot Security, JSP.

Java 8, Spring Boot 2.2.1, Gradle 4.10.2

## Example with Source Code

We will now move on to Spring Boot Security Example – Single Sign On using OAuth 2.

First we need to configure OAuth 2 in Github, once it is done then we will create Spring Boot Security Example in Eclipse.

### Configure OAuth2 App in Github

Login to your Github profile section.

Then click on ***Settings*** from dropdown menu by clicking on your profile picture on top right corner. 
Next click on ***Developer settings***. The below figure shows the highlighted menu:

![Github Settings](img/settings.png "Github Settings")

![Github Developer Settings](img/developer.png "Github Developer Settings")

Now click on New OAuth App as shown in the below figure to register a new application to use OAuth:

![Github App Settings](img/githubapp.png "Github App Settings")

Now below screen appears where you have to put the values for the input fields as per your requirements:

![Register Github App Settings](img/register.png "Register Github App Settings")

As you see in the above figure I have entered values for the input fields and as I want to show you how it works, so I have just created this app to test our example.

The most of the fields are self-explanatory. The Authorization callback URL is the URL, where you want to redirect your users after authorization was successful.

Once you click on Register application, your application will be created and you will be redirected to a page, where Client ID and Client Secret get generated. The below figure shows similar page:

The Client ID and Client Secret are required to authenticate your app.

Testing the Application

When you hit the URL http://localhost:8080 then user will see below page:

![Index Page](img/index.png "Index Page")

So user is not able to see the home page of the URL and user needs to authenticate using Github account.

Now when user clicks on click here link, then user will be redirected to the Github login page:

![Auth Page](img/auth.png "Auth Page")

So now user will be able to see the Logout button as the user has been authorized.

Hope you got idea on Spring Boot Security Example – Single Sing On using OAuth 2.